#!/bin/bash

# Install Modules
npm install --prefx ./modules/bitrading-module-manager

# Backend install
rm -rf backend/local_modules
cp -r modules/ backend/local_modules
# rm -rf backend/local_data
# cp -r data/ backend/local_data
npm install --prefix ./backend

rm -rf backend/node_modules/bitrading-module-manager
cp -r modules/bitrading-module-manager backend/node_modules/bitrading-module-manager
