#!/bin/bash

# Update git submodule
git submodule init
git submodule sync
git submodule update --remote

docker build --force-rm . -t template_installer:latest
docker run --rm -v "`pwd`:/template" template_installer:latest
docker rmi template_installer:latest

cp .env-example .env
docker-compose up --build -d
