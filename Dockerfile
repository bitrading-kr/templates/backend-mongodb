# Not use this dockerfile in docker-compose
FROM node:8.11
RUN mkdir /template
WORKDIR /template
VOLUME /template

CMD [ "./Install.modules.sh" ]
